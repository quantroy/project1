package com.fsdproject.employeeSystem.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Employee {
    private int id;

    private String name;
    private String address;
    private String email;
    private String phoneNumber;
    private String salary;
}
