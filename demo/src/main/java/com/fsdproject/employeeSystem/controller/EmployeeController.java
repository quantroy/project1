package com.fsdproject.employeeSystem.controller;

import com.fsdproject.employeeSystem.model.Employee;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class EmployeeController {
    @GetMapping("/index")
    public String home(){
        return "index";
    }
    @GetMapping("/addemp")
    public String addEmpForm(){
        return "add_emp";
    }
    @PostMapping("/register")
    public String empRegister(@ModelAttribute Employee employee){

        System.out.println(employee);
        return "";
    }
}
